package com.quannt.chapter09;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.net.URL;
import java.util.stream.*;

public final class Main {


    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 9 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);

            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            } catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            } catch (Error error) {
                System.out.println("An error has occurred " + error);
            }
            switch (questionNumber) {
                case 1:
                    question01();
                    break;
                case 2:
                    question02();
                    break;
                case 4:
                    try {
                        question04();
                    }
                    catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 8:
                    try {
                        question08(in);
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                case 13:
                    question13();

                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }



    private static void question01() {
        String file1Path = "/Users/quannt/Documents/test1";
        String file2Path = "/Users/quannt/Documents/test2";
        String file3Path = "/Users/quannt/Documents/test3";
        try {
            // Method 1
            URL url = new URL("https://www.google.com/");
            InputStream in = url.openStream();
            OutputStream out = copy(in);

            // Method 2
            InputStream in2 = url.openStream();
            OutputStream out2 = copyUsingFile(in2);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static OutputStream copy(InputStream in) throws IOException
    {
        OutputStream out = new ByteArrayOutputStream();
        final int BLOCKSIZE = 1024;
        byte[] bytes = new byte[BLOCKSIZE];
        int len;
        while ((len = in.read(bytes)) != -1) {
            out.write(bytes, 0, len);
        }
        return out;
    }

    private static OutputStream copyUsingFile(InputStream in) throws IOException
    {
        OutputStream finalResult = new ByteArrayOutputStream();
        Path tempFile = Files.createTempFile(null, null);
        Files.copy(in, tempFile);
        Files.copy(tempFile, finalResult);
        return finalResult;
    }

    private static void question02() {
        String file1Path = "/Users/quannt/Documents/test1";
        Path input = Paths.get(file1Path);
        String inputFileName = input.getFileName().toString();
        Path output = Paths.get("/Users/quannt/Documents/" + inputFileName + ".toc");

        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(output))) {
            List<String> lines = Files.readAllLines(input, StandardCharsets.UTF_8);
            lines.forEach(writer::println);
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }

    private static void question04() throws IOException {
        String file1Path = "/Users/quannt/Documents/test1";

        long start = System.currentTimeMillis();
        Scanner inScanner = new Scanner(Paths.get(file1Path), "UTF-8");
        Integer countLineScanner = 0;
        while (inScanner.hasNextLine()) {
            String line = inScanner.nextLine();
            countLineScanner++;
        }


        System.out.println("Count lines using scanner: " + String.valueOf(countLineScanner));
        System.out.println("In " + (System.currentTimeMillis() - start));

        Long countLineBufferedReader;
        start = System.currentTimeMillis();
        try (BufferedReader reader
                     = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get(file1Path))))) {
            Stream<String> lines = reader.lines();
            countLineBufferedReader = lines.count();
        }
        System.out.println("Count lines using BufferedReader: " + String.valueOf(countLineBufferedReader));
        System.out.println("In " + (System.currentTimeMillis() - start));

        Long countLineFileBufferedReader;
        start = System.currentTimeMillis();
        try (Stream<String> stream = Files.newBufferedReader(Paths.get(file1Path)).lines()) {
            countLineFileBufferedReader = stream.count();
        }
        System.out.println("Count lines using bufferedReader: " + String.valueOf(countLineFileBufferedReader));
        System.out.println("In " + (System.currentTimeMillis() - start));
    }

    /**
     *
     * a utility method for producing a ZIP file containing all files from a directory
     and its descendants.
     */
    private static void question08(Scanner in) throws URISyntaxException, IOException {
        // System.out.println("Type in directory");
        String directory = "/Users/quannt/Documents/Wallpaper";
        Random randomSeed = new Random();

        Path zipPath = Paths.get("/Users/quannt/Documents/myZip" + randomSeed.nextInt() + ".zip");
        URI uri = new URI("jar", zipPath.toUri().toString(), null);


        try (FileSystem zipfs = FileSystems.newFileSystem(uri, Collections.singletonMap("create", "true"))) {
            try (Stream<Path> entries = Files.walk(Paths.get(directory))) {
                entries.forEach(p -> {
                    if (Files.isRegularFile(p)) {
                        try {
                            Files.copy(p, zipfs.getPath("/").resolve(p.getFileName().toString()));
                        }
                        catch (IOException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                });
            }
        }
    }

    private static void question13() {
        Employee e1 = new Employee("Quan");

        // Serialize the object
        ObjectOutputStream out = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            out = new ObjectOutputStream(byteArrayOutputStream);
            out.writeObject(e1);
            out.flush();

            byte[] outputByte = byteArrayOutputStream.toByteArray();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputByte);
            ObjectInputStream in = new ObjectInputStream(byteArrayInputStream);

            Employee e2 = (Employee) in.readObject();

            System.out.println(e1);
            System.out.println(e2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
