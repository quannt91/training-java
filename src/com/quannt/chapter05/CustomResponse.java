package com.quannt.chapter05;
import java.util.ArrayList;

public class CustomResponse<T> {
    public Integer statusCode;
    public String errorMessage;
    public Exception exception;
    public ArrayList<Double> list;
    public Double sum;
    public T payload;

    public CustomResponse() {this(0, "", null, null);}

    public CustomResponse(Integer statusCode, String errorMessage, Exception exception, T payload) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        this.exception = exception;
        this.payload = payload;
    }
}
