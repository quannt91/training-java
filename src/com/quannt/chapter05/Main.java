package com.quannt.chapter05;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.*;

public final class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 5 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);
            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            }
            catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            }
            catch (Error error){

            }
            switch (questionNumber) {
                case 1:
                    question01(in);
                    break;
                case 2:
                    question02(in);
                    break;
                case 4:
                    question04(in);
                    break;
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01(Scanner in) {
        System.out.println("Please type in the file path");
        String filePath = in.next();

        ArrayList<Double> result;
        try {
            result = readValues(filePath);
            System.out.println(Arrays.toString(result.toArray()));
        }
        catch (IOException | IllegalArgumentException ex) {
            // Logger.getGlobal().fine(ex.toString());
            ex.printStackTrace();
        }
    }



    private static void question02(Scanner in) {
        System.out.println("Please type in the file path");
        String filePath = in.next();
        Double result;

        try {
            result = sumOfValues(filePath);
            System.out.println(String.valueOf(result));
        }
        catch (IOException | IllegalArgumentException ex) {
            Logger.getGlobal().fine(ex.toString());
            System.out.println("An error has occurred: " + ex.getMessage());
        }
    }

    private static void question04(Scanner in) {
        System.out.println("Please type in the file path");
        String filePath = in.next();

        CustomResponse response = sumOfValuesNoException(filePath);
        if (response.statusCode.equals(1)) {
            System.out.println(response.errorMessage + ' ' + response.exception);
            return;
        }
        System.out.println(String.valueOf(response.sum));
    }

    /**
     * @param filePath file location
     * @return return the list of numbers
     * @throws IOException throw if file cannot be opened
     * @throws IllegalArgumentException throw if input are not floating points
     */
    private static ArrayList<Double> readValues(String filePath) throws IOException {
        // This will reference one line at a time
        String line;
        ArrayList<Double> result = new ArrayList<>();
        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while((line = bufferedReader.readLine()) != null) {
            try{
                result.add(Double.parseDouble(line));
            }
            catch (Exception ignored) {
                throw new IllegalArgumentException("Input is not valid floating number " + String.valueOf(line));
            }
        }
        bufferedReader.close();
        return result;
    }

    private static double sumOfValues(String filePath) throws IOException {
        Double sum = 0d;
        for (Double aDouble : readValues(filePath)) {
            sum += aDouble;
        }
        return sum;
    }

    private static <T> CustomResponse readValuesNoException(String filePath) {
        // This will reference one line at a time
        String line;
        ArrayList<Double> result = new ArrayList<>();
        FileReader fileReader;
        try {
            fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                try {
                    result.add(Double.parseDouble(line));
                } catch (Exception ex) {
                    return new CustomResponse<>(1, "Input is not valid floating number " + String.valueOf(line),
                                                ex, null);
                }
            }
        } catch (IOException ex) {
            return new CustomResponse<>(1, "Error opening file.", ex, null);
        }
        return new CustomResponse<>(0, "", null, result);
    }

    private static <T> CustomResponse sumOfValuesNoException(String filePath){
        Double sum = 0d;
        CustomResponse response = readValuesNoException(filePath);
        if (response.statusCode.equals(1)) {
            return new CustomResponse<>(1, "Error calculating the sum", response.exception, null);
        }

//        for (T aDouble : response.payload) {
//            sum += (Double)aDouble;
//        }
        return new CustomResponse<>(0, "", null, sum);
    }
}
