package com.quannt.chapter12;


import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 12 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);

            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            } catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            } catch (Error error) {
                System.out.println("An error has occurred " + error);
            }
            switch (questionNumber) {
                case 1:
                    try {
                        question01();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 2:
                    question02();
                    break;
                case 5:
                    question05();
                    break;
                case 6:
                    question06();
                    break;
                case 7:
                    question07();
                    break;
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01() {
        LocalDate programmersDay = LocalDate.of(2014,1,1).withDayOfYear(256);
        System.out.println(programmersDay);
    }

    private static void question02() {
        LocalDate input = LocalDate.of(2000, 2, 29).plusYears(1);
        System.out.println(input);
    }

    private static void question05() {
        LocalDate myBirthday = LocalDate.of(1991, 4, 9);
        LocalDate today = LocalDate.now();

        System.out.println("I have been alive for " + myBirthday.until(today, ChronoUnit.DAYS) + " days.");

    }

    private static void question06() {
        LocalDate start = LocalDate.of(1901, 1, 1);
        LocalDate end = LocalDate.of(2000, 12, 31);

        TemporalAdjuster NEXT_FRIDAY_13TH = w -> {
            LocalDate result;
            do {
                result = (LocalDate) w.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
                w = result;
            } while (result.getDayOfMonth() != 13);
            return result;
        };



        for (LocalDate date = start; date.isBefore(end); date = date.plusDays(1))
        {
            LocalDate first13thFriday = date
                    .with(NEXT_FRIDAY_13TH);

            // Set date to the start of next month to save time
            date = first13thFriday.with(TemporalAdjusters.firstDayOfNextMonth());

            System.out.println(first13thFriday);
        }
    }

    private static void question07() {
        TimeInterval t1 = new TimeInterval(
                ZonedDateTime.of(2011,1,1,15,0,0,0, ZoneId.of("America/New_York")),
                ZonedDateTime.of(2016,1,1,15,0,0,0, ZoneId.of("America/New_York")));

        TimeInterval t2 = new TimeInterval(
                ZonedDateTime.of(2012,1,1,15,0,0,0, ZoneId.of("America/New_York")),
                ZonedDateTime.of(2018,1,1,15,0,0,0, ZoneId.of("America/New_York")));


        System.out.println("Interval 1 from " + t1.start + " to " + t1.end);
        System.out.println("Interval 2 from " + t2.start + " to " + t2.end);
        System.out.println(isTimeIntervalOverlapped(t1, t2));


        TimeInterval t3 = new TimeInterval(
                ZonedDateTime.of(2011,1,1,15,0,0,0, ZoneId.of("America/New_York")),
                ZonedDateTime.of(2016,1,1,15,0,0,0, ZoneId.of("America/New_York")));

        TimeInterval t4 = new TimeInterval(
                ZonedDateTime.of(3000,1,1,15,0,0,0, ZoneId.of("America/New_York")),
                ZonedDateTime.of(4000,1,1,15,0,0,0, ZoneId.of("America/New_York")));


        System.out.println("Interval 3 from " + t3.start + " to " + t3.end);
        System.out.println("Interval 4 from " + t4.start + " to " + t4.end);
        System.out.println(isTimeIntervalOverlapped(t4, t3));

    }

    private static boolean isTimeIntervalOverlapped(TimeInterval t1, TimeInterval t2) {
        return !(t1.end.isBefore(t2.start) || t1.start.isAfter((t2.end)));
    }
}
