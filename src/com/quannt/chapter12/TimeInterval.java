package com.quannt.chapter12;

import java.time.ZonedDateTime;

public class TimeInterval {
    public ZonedDateTime start;
    public ZonedDateTime end;


    public TimeInterval(ZonedDateTime start, ZonedDateTime end) {
        this.start = start;
        this.end = end;
    }
}
