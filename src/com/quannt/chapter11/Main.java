package com.quannt.chapter11;

import java.io.IOException;
import java.util.Scanner;

public final class Main {


    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 10 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);

            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            } catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            } catch (Error error) {
                System.out.println("An error has occurred " + error);
            }
            switch (questionNumber) {
                case 1:
                    try {
                        question01();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01() throws IOException {
        // Person personA = new Person();
        // do the clone here

    }

    @Cloneable(false)
    public class Person {
        Integer age;
        String name;
    }
}
