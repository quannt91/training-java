package com.quannt.chapter11;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Serializable {
    boolean value() default true;
}


//public class Serializables {
//    public static Object clone(Object obj) throws CloneNotSupportedException {
//        if (obj == null) return null;
//        Class<?> cl = obj.getClass();
//        Cloneable ts = cl.getAnnotation(Cloneable.class);
//        if (ts == null) throw new CloneNotSupportedException();
//
//        if (ts.value()) {
//            //return (Object) obj.clone(); do the actual cloning
//            return obj;
//        }
//    }
//}