package com.quannt.chapter07;
import java.util.*;


public final class Main {
    enum Weekday { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    };

    public static void main(String[] args) {




        while (true) {
            System.out.println("Chapter 7 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);



            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            } catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            } catch (Error error) {
                System.out.println("An error has occurred " + error);
            }
            switch (questionNumber) {
                case 1:
                    question01(in);
                    break;
                case 2:
                    question02(in);
                    break;
                case 3:
                    question03(in);
                    break;
                case 4:
                    question04();
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01(Scanner in) {
        System.out.println("Please type in the number: ");
        Integer n = in.nextInt();
        System.out.println("Finding all prime numbers from 2 to " + String.valueOf(n) + " using HashSet");
        // Using HashSet
        Set<Integer> inputs = new HashSet<>();
        // Populate the input list
        for (Integer i = 2; i <= n; i++) {
            inputs.add(i);
        }
        for (Integer smallest = 0; smallest <= n; smallest++) {
            for (Integer j = 2; j <= n; j++) {
                inputs.remove(j * (j + smallest));
            }
        }
        System.out.println(inputs);

        System.out.println("Finding all prime numbers from 2 to " + String.valueOf(n) + " using BitSet");
        // Using BitSet
        BitSet inputBitSet = new BitSet();
        // Populate the input list
        for (Integer i = 2; i <= n; i++) {
            inputBitSet.set(i);
        }
        for (Integer smallest = 0; smallest <= n; smallest++) {
            for (Integer j = 2; j <= n; j++) {
                inputBitSet.clear(j * (j + smallest));
            }
        }
        System.out.println(inputs);
    }

    private static void question02(Scanner in) {
        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("fooBAr");
        inputs.add("foo");
        inputs.add("bar");

        System.out.printf("Input %s%n", inputs.toString());
        Iterator<String> iterator = inputs.iterator();

        // Using iterator
        ArrayList<String> output1 = new ArrayList<>();
        while (iterator.hasNext()) {
            output1.add(iterator.next().toUpperCase());
        }
        System.out.printf("Output 1 %s%n", output1.toString());

        // Using for loop
        ArrayList<String> output2 = new ArrayList<>();
        for (String input : inputs) {
            output2.add(input.toUpperCase());
        }
        System.out.printf("Output 2 %s%n", output2.toString());


        // Using replace all
        ArrayList<String> output3 = new ArrayList<>(inputs);
        output3.replaceAll(String::toUpperCase);
        System.out.printf("Output 3 %s%n", output3.toString());
    }

    private static void question03(Scanner in) {
        Set<Integer> input01 = new HashSet<>();
        for (Integer i = 0; i < 10; i++) {
            input01.add(i);
        }

        Set<Integer> input02 = new HashSet<>();
        for (Integer i = 6; i < 20; i++) {
            input02.add(i);
        }

        System.out.printf("Set 01 %s%n", input01.toString());
        System.out.printf("Set 02 %s%n", input02.toString());
        // Union
        input01.addAll(input02);
        System.out.printf("Union is %s%n", input01);

        // Intersection
        input01.clear();
        for (Integer i = 0; i < 10; i++) {
            input01.add(i);
        }

        input01.retainAll(input02);
        System.out.printf("Intersection is %s%n", input01);

        // difference
        input01.clear();
        for (Integer i = 0; i < 10; i++) {
            input01.add(i);
        }

        input01.removeAll(input02);
        System.out.printf("Difference is %s%n", input01);
    }

    private static void question04() {
        ArrayList<Integer> input = new ArrayList<>();
        for (Integer i = 0; i < 10; i++) {
            input.add(i);
        }

        for (Integer i : input) {
            try {
                input.remove(i);
            }
            catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }
}
