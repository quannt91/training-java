package com.quannt.chapter01;

import java.math.BigInteger;
import java.util.Scanner;

public final class  Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 1 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);
            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            }
            catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            }
            switch (questionNumber) {
                case 1:
                    question01(in);
                    break;
                case 2:
                    question02(in);
                    break;
                case 3:
                    question03(in);
                    break;
                case 6:
                    question06(in);
                    break;
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01(Scanner in) {
        System.out.println("Please type in the integer number: ");
        Integer input = 0;
        try {
            input = in.nextInt();
        }
        catch (Exception ignored) {
            System.out.println("Invalid integer number.");
        }
        System.out.println("Binary value is " + Integer.toBinaryString(input));
        System.out.println("Octal value is " + Integer.toOctalString(input));
        System.out.println("Hex value is " + Integer.toHexString(input));
        if (input == 0) {
            return;
        }
        Double reciprocal = Double.valueOf(1d / input);
        System.out.println("Hex value of reciprocal is " + Double.toHexString(reciprocal));
    }

    private static void question02(Scanner in) {
        System.out.println("Please type in the integer number: ");
        Integer input = 0;
        try {
            input = in.nextInt();
        }
        catch (Exception ignored) {
            System.out.println("Invalid integer number.");
        }

        Integer output = Math.abs(input) % 360;
        Integer output2 = Math.floorMod(Math.abs(input), 360);
        System.out.println("Normalized angle using % is " + String.valueOf(output));
        System.out.println("Normalized angle using floorMod is " + String.valueOf(output2));
    }

    private static void question03(Scanner in) {
        System.out.println("Please type in 3 integer numbers: ");
        Integer input01 = 0;
        Integer input02 = 0;
        Integer input03 = 0;
        try {
            input01 = in.nextInt();
            input02 = in.nextInt();
            input03 = in.nextInt();
        }
        catch (Exception ignored) {
            System.out.println("Invalid integer number.");
        }

        Integer output = Math.max(Math.max(input01, input02), input03);
        System.out.println("Largest number is " + String.valueOf(output));
    }

    private static void question06(Scanner in) {
        System.out.println("Please type in the integer number: ");
        Integer input = 0;
        try {
            input = in.nextInt();
        }
        catch (Exception ignored) {
            System.out.println("Invalid integer number.");
        }

        BigInteger output = BigInteger.valueOf(1);
        for (Integer i = 1; i <= input; i++) {
            output = output.multiply(BigInteger.valueOf(i));
        }

        System.out.println("Factorial is " + String.valueOf(output));
    }

}
