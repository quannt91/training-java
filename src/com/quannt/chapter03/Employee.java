package com.quannt.chapter03;

public class Employee implements Measurable {
    private final double salary;
    private final String name;

    public Employee(double salary) {
        this.salary = salary;
        name = "Employee_" + String.valueOf(salary);
    }

    @Override
    public double getMeasure() {
        return salary;
    }

    public String getName() {
        return name;
    }
}
