package com.quannt.chapter03;

/**
 * Interface that measure the object in some ways
 */
@FunctionalInterface
public interface Measurable {
    /**
     *
     * @return return the measurement value in double
     */
    double getMeasure();
}
