package com.quannt.chapter03;

import java.util.ArrayList;
import java.util.Scanner;

public final class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 3 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);
            Integer questionNumber = 0;

            ArrayList<Integer> foo = new ArrayList<>();
            foo.toArray(new Integer[]{1});

            try {
                questionNumber = in.nextInt();
            }
            catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            }
            switch (questionNumber) {
                case 1:
                    question01(in);
                    break;
                case 2:
                    question02(in);
                    break;
                case 3:
                    question03(in);
                    break;
                case 4:
                    question04(in);
                    break;
                case 5:
                    question05(in);
                    break;
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01(Scanner in) {
        System.out.println("Type in salary of 5 employees");
        Employee[] employees = new Employee[5];
        try {
            for (int i = 0; i < 5; i++) {
                employees[i] = new Employee(in.nextInt());
            }
        }
        catch (Exception ignored) {
            System.out.println("Invalid exercise.");
        }

        double averageSalary = average(employees);
        System.out.println("Average salary is " + String.valueOf(averageSalary));

    }

    private static void question04(Scanner in) {

    }

    private static void question03(Scanner in) {

    }

    private static void question02(Scanner in) {
        System.out.println("Type in salary of 5 employees");
        Employee[] employees = new Employee[5];
        try {
            for (int i = 0; i < 5; i++) {
                employees[i] = new Employee(in.nextInt());
            }
        }
        catch (Exception ignored) {
            System.out.println("Invalid exercise.");
        }

        largest(employees);
    }

    private static void question05(Scanner in) {

    }

    private static double average(Measurable[] objects) {
        if (objects.length == 0) {
            return 0;
        }
        double totalSalary = 0;
        for (Measurable object : objects) {
            totalSalary += object.getMeasure();
        }
        return totalSalary / objects.length;
    }

    private static void largest(Measurable[] objects) {
        if (objects.length == 0) {
            return;
        }
        double maxSalary = 0;
        String maxSalaryEmployeeName = "";
        for (Measurable object : objects) {
            if (object.getMeasure() >= maxSalary) {
                maxSalary = object.getMeasure();
                Employee employee = (Employee) object;
                maxSalaryEmployeeName = employee.getName();
            }
        }

        System.out.println("Employee with largest salary is " + maxSalaryEmployeeName);
    }
}

