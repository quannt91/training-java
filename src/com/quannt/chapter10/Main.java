package com.quannt.chapter10;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Main {


    public static void main(String[] args) {
        while (true) {
            System.out.println("Chapter 10 exercise - Ngo Tung Quan");
            System.out.println("Please type in the number of the exercise you want to execute: ");
            Scanner in = new Scanner(System.in);

            Integer questionNumber = 0;
            try {
                questionNumber = in.nextInt();
            } catch (Exception ignored) {
                System.out.println("Invalid exercise.");
            } catch (Error error) {
                System.out.println("An error has occurred " + error);
            }
            switch (questionNumber) {
                case 1:
                    try {
                        question01();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 2:
                    question02();
                case 5:
                    try {
                        question05();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                case 6:
                    try {
                        question06();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                case 8:
                    try {
                        question08();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                default:
                    System.out.println("Exercise is not available.");
                    break;
            }
        }
    }

    private static void question01() throws IOException {
        String directory = "/Users/quannt/Documents/Wallpaper";
        String keyword = "own";
        Collection<Path> result = new LinkedList<>();
        Stream<Path> entries = Files.walk(Paths.get(directory));
        entries.parallel().filter(entry -> Files.isRegularFile(entry) && entry.getFileName().toString().contains(keyword)).forEach(result::add);
        Arrays.stream(result.toArray()).forEach(System.out::println);

        // Print first
        Path firstResult = (Path) result.toArray()[0];
        System.out.println(firstResult.getFileName());
    }

    private static void question02() {
        sortArray(10);
        sortArray(100);
        sortArray(10000);
        sortArray(100000);
    }

    private static void sortArray(Integer count) {
        int[] input = new int[count];
        Random random = new Random();
        for (int i = 0; i < input.length; i++) {
            input[i] = random.nextInt();
        }
        long start = System.currentTimeMillis();
        Arrays.sort(input);
        System.out.println("Sorted using Arrays.sort with " + count + " elements in " + (System.currentTimeMillis() - start));

        for (int i = 0; i < input.length; i++) {
            input[i] = random.nextInt();
        }
        start = System.currentTimeMillis();
        Arrays.parallelSort(input);
        System.out.println("Sorted using Arrays.parallelSort with " + count + " elements in " + (System.currentTimeMillis() - start));
    }

    private static void question05() throws IOException {
        String directory = "/Users/quannt/Documents/text";

        Stream<Path> paths = Files.walk(Paths.get(directory)).parallel();
        ConcurrentHashMap<String, Set<File>> bookKeeping = new ConcurrentHashMap<>();

        paths.forEach(p -> {
            try {
                Scanner scanner = new Scanner(p, "UTF-8");
                // scanner.useDelimiter("\PL+");
                while (scanner.hasNext()) {
                    String word = scanner.next();
                    Set<File> files = new HashSet<>();
                    files.add(p.toFile());
                    bookKeeping.merge(word, files, (existingValue, newValue) ->  Stream.concat(existingValue.stream(), newValue.stream()).collect(
                            Collectors.toSet()));
                }

                bookKeeping.forEach((word, files) -> System.out.println("Word: " + word + ", files " + files));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private static void question06() throws IOException {
        String directory = "/Users/quannt/Documents/text";

        Stream<Path> paths = Files.walk(Paths.get(directory)).parallel();
        ConcurrentHashMap<String, Set<File>> bookKeeping = new ConcurrentHashMap<>();

        paths.forEach(p -> {
            try {
                Scanner scanner = new Scanner(p, "UTF-8");
                while (scanner.hasNext()) {
                    String word = scanner.next();
                    Set<File> files = new HashSet<>();
                    files.add(p.toFile());

                    bookKeeping.computeIfAbsent(word, k -> {
                        Set<File> newSet = new HashSet<>();
                        newSet.add(p.toFile());
                        return newSet;
                    }).add(p.toFile());
                }

                bookKeeping.forEach((word, files) -> System.out.println("Word: " + word + ", files " + files));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private static void question08() {

        long start = System.currentTimeMillis();
        AtomicLong counter = new AtomicLong();
        for (int i = 0; i < 1000; i++) {
            Runnable task = () -> {
                for (int j = 0; j < 100_000; j++) {
                    counter.incrementAndGet();
                }
            };
            Thread thread = new Thread(task);
            thread.start();
        }
        System.out.println(". Computed in " + (System.currentTimeMillis() - start) + " ms.");


        long start2 = System.currentTimeMillis();
        final LongAdder counter2 = new LongAdder();
        for (int i = 0; i < 1000; i++) {
            Runnable task = () -> {
                for (int j = 0; j < 100_000; j++) {
                    counter2.increment();
                }
            };
            Thread thread = new Thread(task);
            thread.start();
        }
        System.out.println(". Computed in " + (System.currentTimeMillis() - start) + " ms.");
    }
}
